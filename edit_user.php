<?php
include_once('./condb.php');

$uid = $_GET['uid'];
$finduser = "SELECT * FROM `users` WHERE `u_id` = '$uid'";
$qfinduser = mysqli_query($conn, $finduser);
$rfinduser = mysqli_fetch_assoc($qfinduser);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit | Onclass php</title>
    <!-- Bootstrap 4 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <!-- Fontawesome icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>

<body>

    <div class="container">
        <div class="row">
            <!-- Form insert data -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            <i class="fas fa-user-edit"></i> Edit -> <?= $rfinduser['u_username']; ?>
                        </h5>
                        <form action="./function.php" method="post">
                            <input type="hidden" class="form-control-plaintext" name="userid" value="<?= $uid; ?>" readonly>
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="<?= $rfinduser['u_email']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="text" name="username" id="username" class="form-control" placeholder="Username" value="<?= $rfinduser['u_username']; ?>" required>
                            </div>

                            <div class="form-group">
                                <input type="password" name="pwd" id="pwd" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary btn-block" name="edituser">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <!-- Bootstrap 4 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <!-- External js -->
    <script src="./assets/js/script.js"></script>
</body>

</html>