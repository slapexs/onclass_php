<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Shopiew</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="mainNavbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=users">List users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=producttypes">Product types</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?page=products">Products</a>
                </li>
            </ul>
        </div>
    </div>
</nav>