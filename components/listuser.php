<?php
$get_users = "SELECT * FROM `users`";
$qget_users = mysqli_query($conn, $get_users);
?>

<div>
    <h5>List users</h5>
    <hr>
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr>
                <th class="text-center">ID</th>
                <th>Email</th>
                <th>Username</th>
                <th>action</th>
            </tr>
        </thead>

        <tbody>
            <?php while ($rget_users = mysqli_fetch_array($qget_users)) { ?>
                <tr>
                    <td class="text-center align-middle"><?= $rget_users['u_id']; ?></td>
                    <td class="align-middle"><?= $rget_users['u_email']; ?></td>
                    <td class="align-middle"><?= $rget_users['u_username']; ?></td>
                    <td class="align-middle">
                        <button class="btn btn-sm btn-outline-danger" type="button" onclick="edit_user(<?= $rget_users['u_id']; ?>)"><i class="fas fa-edit"></i></button>
                        <button class="btn btn-sm btn-dark" type="button" onclick="confirm_del(<?= $rget_users['u_id']; ?>)"><i class="fas fa-trash"></i></button>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>