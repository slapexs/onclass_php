<?php
$hostname = "localhost";
$hostusername = "root";
$hostpassword = "";
$database = "study";

$conn = mysqli_connect($hostname, $hostusername, $hostpassword, $database) or die("Error: " . mysqli_error($conn));
$qconn = mysqli_query($conn, "SET NAMES 'utf-8'");
date_default_timezone_set('Asia/Bangkok');