<?php
error_reporting(0);
include_once('./condb.php');

// Insert user
if (isset($_POST['adduser'])) {
    $username = $_POST['username'];
    $pwd = $_POST['pwd'];
    $email = $_POST['email'];

    $hash_pwd = password_hash($pwd, PASSWORD_DEFAULT);
    $ins = "INSERT INTO `users` (`u_username`, `u_password`, `u_email`) VALUES ('$username', '$hash_pwd', '$email')";
    $qins = mysqli_query($conn, $ins);
    if ($qins) {
        echo 'Data inserted';
        echo '<a href="./">Home</a>';
    } else {
        echo 'Fail to insert data';
    }
    mysqli_close($conn);
}

// Edit user
if (isset($_POST['edituser'])) {
    $username = $_REQUEST['username'];
    $pwd = $_REQUEST['pwd'];
    $email = $_REQUEST['email'];
    $uid = $_POST['userid'];
    $hash_pwd = password_hash($pwd, PASSWORD_DEFAULT);

    $update = "UPDATE `users` SET `u_username` = '$username', `u_email` = '$email', `u_password` = '$hash_pwd' WHERE `u_id` = '$uid'";
    $qupdate = mysqli_query($conn, $update);
    if ($qupdate) {
        echo 'User updated <br>';
        echo '<a href="./">Home</a>';
    } else {
        echo 'User not updated';
    }

    mysqli_close($conn);
}


// Add product type
if (isset($_POST['addproducttype'])) {
    $pd_name = $_POST['product_type'];

    // Check name already
    $findpdname = "SELECT * FROM `product_types` WHERE `pd_name` = '$pd_name'";
    $qfindpdname = mysqli_query($conn, $findpdname);
    $cfindinspdname = mysqli_num_rows($qfindpdname);

    if ($cfindinspdname <= 0) {
        $inspd = "INSERT INTO `product_types` (`pd_name`) VALUES ('$pd_name')";
        $qinspd = mysqli_query($conn, $inspd);
        if ($qinspd) {
            echo "<script>
        alert('เพิ่มประเภทสินค้าเรียบร้อย');
        window.location.href = './?page=producttypes';
        </script>";
        }
    } else {
        echo "<script>
        alert('ประเภทสินค้านี้มีในระบบแล้ว ไม่สามารถเพิ่มซ้ำได้');
        window.location.href = './?page=producttypes';
        </script>";
    }
}

// Add product
if (isset($_POST['addproduct'])) {
    $product_name = $_POST['product_name'];
    $product_type = $_POST['product_type'];
    $product_price = $_POST['product_price'];
    $product_img = $_FILES['product_img']['name'];

    // Check product name already
    $findp = "SELECT * FROM `products` WHERE `p_name` = '$product_name'";
    $qfindp = mysqli_query($conn, $findp);
    $cfindp = mysqli_num_rows($qfindp);
    if ($cfindp <= 0) {
        // image change image name and set location
        $typeimg = strrchr($_FILES['product_img']['name'], '.');
        $img_newname = "product_" . date('dmY') . "_" . time() . $typeimg;
        $location = './upload/image/' . $img_newname;

        move_uploaded_file($_FILES['product_img']['tmp_name'], $location);

        $insp = "INSERT INTO `products` (`p_name`, `p_ref_typeid`, `p_img`, `p_price`) VALUES ('$product_name', '$product_type', '$img_newname', '$product_price')";
        $qinsp = mysqli_query($conn, $insp);
        if ($qinsp) {
            echo "<script>
            alert('เพิ่มสินค้าเรียบร้อย');
            window.location.href = './?page=products';
            </script>";
        } else {
            echo "<script>
            alert('ไม่สามารถเพิ่มสินค้าได้');
            window.location.href = './?page=products';
            </script>";
        }
    } else {
            echo "<script>
            alert('ไม่สามารถเพิ่มสินค้าซ้ำได้');
            window.location.href = './?page=products';
            </script>";
    }
}
