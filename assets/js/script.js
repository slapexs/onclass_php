function confirm_del(uid) {
  let stateConfirm = confirm("Confirm to delete?");
  if (stateConfirm) {
    window.location.href = `./del_user.php?uid=${uid}`
  }
}

function edit_user(uid){
  window.location.href = `./edit_user.php?uid=${uid}`
}
