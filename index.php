<?php
// Rm02S21DOVpjd2dudzJ4cFpFeXE=
include_once('./condb.php');
$page = $_GET['page'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Onclass php</title>
    <!-- Bootstrap 4 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <!-- Fontawesome icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <!-- External css -->
    <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>
    <?php include_once('./components/navbar.php'); ?>

    <div class="container">
        <?php
        if ($page == '') {
            include_once('./pages/list_users.php');
        } else if ($page == 'producttypes') {
            include_once('./pages/product_type.php');
        }else if ($page == 'products'){
            include_once('./pages/products.php');
        }
        ?>
    </div>


    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <!-- Bootstrap 4 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <!-- External js -->
    <script src="./assets/js/script.js"></script>
</body>

</html>