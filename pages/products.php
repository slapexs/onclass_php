<?php
$findproduct  = "SELECT * FROM `products` as p INNER JOIN `product_types` as pd ON pd.pd_id = p.p_ref_typeid";
$qfindproduct = mysqli_query($conn, $findproduct);

$findpd = "SELECT * FROM `product_types`";
$qfindpd = mysqli_query($conn, $findpd);
?>

<div class="row mt-3">
    <div class="col-lg-6">
        <div class="bg-light rounded p-3">
        <h4><i class="fas fa-plus"></i> เพิ่มสินค้า</h4>
            <hr>

            <form action="./function.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="product_name">ชื่อสินค้า</label>
                    <input type="text" name="product_name" id="product_name" class="form-control" placeholder="Product" required>
                </div>

                <div class="form-group">
                    <label for="product_type">ประเภทสินค้า</label>
                    <select name="product_type" id="product_type" class="form-control" required>
                        <option value="0" selected hidden disabled>-- เลือกประเภท</option>
                        <?php while($rfindpd = mysqli_fetch_array($qfindpd)){ ?>
                        <option value="<?= $rfindpd['pd_id']; ?>"><?= $rfindpd['pd_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="product_price">ราคา</label>
                    <input type="number" name="product_price" id="product_price" class="form-control" placeholder="Price" min="1" value="0" required>
                </div>

                <div class="form-group">
                    <label for="product_img">รูปภาพ</label>
                    <input type="file" name="product_img" id="product_img" class="form-control-file" placeholder="Image" required>
                </div>
                <button class="btn btn-primary" name="addproduct" type="submit">Submit</button>
            </form>
        </div>
    </div>

    <!-- List product types -->
    <div class="col-lg-6">
        <div class="bg-light rounded p-3">
            <h4>Products</h4>
            <hr>

            <table class="table table-sm table-hover">
                <thead class="thead-dark">
                    <tr class="align-middle text-center">
                        <th>Name</th>
                        <th>Product type</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while ($rfindproduct = mysqli_fetch_array($qfindproduct)) { ?>
                        <tr>
                            <td class="text-center align-middle"><?= $rfindproduct['p_name']; ?></td>
                            <td class="align-middle"><?= $rfindproduct['pd_name']; ?></td>
                            <td class="align-middle"><?= number_format($rfindproduct['p_price'],2); ?>฿</td>
                            <td class="align-middle text-center"><img src="./upload/image/<?= $rfindproduct['p_img']; ?>" alt="product image <?= $rfindproduct['p_id']; ?>" class="img-fluid" width="45px"></td>
                            <td class="text-center align-middle">
                                <a href="#" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-sm btn-dark"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>