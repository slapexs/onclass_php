<?php
$findproduct_type  = "SELECT * FROM product_types";
$qfindproduct_type = mysqli_query($conn, $findproduct_type);
?>

<div class="row mt-3">
    <div class="col-lg-6">
        <div class="bg-light rounded p-3">
        <h4><i class="fas fa-plus"></i> Add new product types</h4>
            <hr>

            <form action="./function.php" method="post">
                <div class="form-group">
                    <label for="product_type">Name of product</label>
                    <input type="text" name="product_type" id="product_type" class="form-control" placeholder="Product" required>
                </div>
                <button class="btn btn-primary" name="addproducttype" type="submit">Submit</button>
            </form>
        </div>
    </div>

    <!-- List product types -->
    <div class="col-lg-6">
        <div class="bg-light rounded p-3">
            <h4>Product types</h4>
            <hr>

            <table class="table table-sm table-hover">
                <thead class="thead-dark">
                    <tr class="align-middle text-center">
                        <th>ID</th>
                        <th>Product name</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while ($rfindproduct_type = mysqli_fetch_array($qfindproduct_type)) { ?>
                        <tr>
                            <td class="text-center"><?= $rfindproduct_type['pd_id']; ?></td>
                            <td><?= $rfindproduct_type['pd_name']; ?></td>
                            <td class="text-center">
                                <a href="#" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="#" class="btn btn-sm btn-dark"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>