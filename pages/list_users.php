<div class="row">
    <!-- Form insert data -->
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">
                    Insert data
                </h5>
                <form action="./function.php" method="post">
                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                    </div>

                    <div class="form-group">
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                    </div>

                    <div class="form-group">
                        <input type="password" name="pwd" id="pwd" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-block" name="adduser">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- List users -->
    <div class="col-lg-6">
        <?php
        include('./components/listuser.php');
        ?>
    </div>
</div>